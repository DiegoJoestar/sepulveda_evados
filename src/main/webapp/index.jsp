<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>sepulveda_evados</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />


        <script> type = "text/javascript" >
                    function textoText() {
                        var info = document.getElementById('textoingresado').value;
                        alert(info);

                    }
        </script>
    </head>
    <body background="resources/imagenfondo.jpg" style="background-size: cover">
        <div class="jumbotron">
            <h4 class="display-4" style="text-align: center">ANIMALES DE LA SELVA</h4>
            <p class="lead" style="text-align: center">Diego Sepulveda</p>
            <hr class="my-4">

            <br>

            <div class="row">
                <div class="col-md-2">

                </div>
                <div class="col-md-2">
                    <h3>Cocodrilo</h3>

                    <div class="card">
                        <img class="card-img-top" src="resources/cocodrilo.jpg">
                        <div class="card-body">
                        </div>
                    </div>
                    <audio id="cocodrilo" src="resources/cocodrilo.mp3"></audio>
                    <a class="btn btn-primary btn-lg" onclick="alert('Este es el sonido de un cocodrilo', document.getElementById('cocodrilo').play())" href="#" role="button">Cocodrilo</a>

                </div>
                <div class="col-md-4">
                </div>
                <div class="col-md-2">
                    <h3>Serpiente</h3>

                    <div class="card">
                        <img class="card-img-top" src="resources/serpiente.jpg">
                        <div class="card-body">
                        </div>
                    </div>
                  
                    <audio id="serpiente" src="resources/serpiente.mp3"></audio>
                    <a class="btn btn-primary btn-lg" onclick="alert('Este es el sonido de una serpiente', document.getElementById('serpiente').play())" href="#" role="button">Serpiente</a>

                </div>
                <div class="col-md-2"> 
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <h3>Leon</h3>
                        <div class="card">
                            <img class="card-img-top" src="resources/leon.jpg">
                            <div class="card-body">
                            </div>
                        </div>
                        <audio id="leon" src="resources/leon.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Este es el sonido de un leon', document.getElementById('leon').play())" href="#" role="button">Leon</a>

                    </div>
                    <div class="col-md-3">
                    </div>

                    <div class="col-md-2">
                        <h3>Elefante</h3>

                        <div class="card">
                            <img class="card-img-top" src="resources/elefante.jpg">
                            <div class="card-body">
                            </div>
                        </div>
                        <audio id="elefante" src="resources/elefante.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Este es el sonido de un elefante', document.getElementById('elefante').play())" href="#" role="button">Elefante</a>

                    </div>
                    <div class="col-md-3">     
                    </div>
                    <div class="col-md-2">     
                        <h3>Conejo</h3>

                        <div class="card">
                            <img class="card-img-top" src="resources/conejo.jpg">
                            <div class="card-body">

                            </div>
                        </div>
                        <audio id="conejo" src="resources/conejo.mp3"></audio>
                        <a class="btn btn-primary btn-lg" onclick="alert('Este es el sonido de un conejo', document.getElementById('conejo').play())" href="#" role="button">Conejo</a>

                    </div>
                    <p class="lead">
                    </p>
                </div>
            </div>
    </body>
</html>
